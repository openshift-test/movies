package com.softwarenation.movies.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.ManyToAny;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Movie {

	@Id
	private String imdbId;
	private String title;
	private String year;
	private String description;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "movie_category", joinColumns = 
	@JoinColumn(name = "movie_id", referencedColumnName = "id"), inverseJoinColumns = 
	@JoinColumn(name = "category_id", referencedColumnName = "id"))
	private List<Category> categories;
	
}
